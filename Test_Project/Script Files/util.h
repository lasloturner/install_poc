////////////////////////////////////////////////////////////////////////////////
//                                                                            
//    File Name:  util.rul                                                   
//                                                                            
//  Description:  Some Win32API utils                                       
//                                                                            
//  Comments:  
//	1._GetComputerName() - return computer name
//	2._NetShareAdd(szShareName, szPath) - Add network share w/o reboot
//	and some triks for converting from MultiByte strings to Unicode                                                                         
////////////////////////////////////////////////////////////////////////////////

#ifndef _UTIL_H_
#define _UTIL_H_

prototype LONG KERNEL32.GetComputerNameA(BYREF STRING, BYREF LONG);
prototype STRING _GetComputerName();

#ifndef MAX_COMPUTERNAME_LENGTH
#define MAX_COMPUTERNAME_LENGTH 31
#endif

#define CP_ACP                    0           // default to ANSI code page
#define CP_OEMCP                  1           // default to OEM  code page
#define CP_MACCP                  2           // default to MAC  code page
#define CP_THREAD_ACP             3           // current thread's ANSI code page
#define CP_SYMBOL                 42          // SYMBOL translations

#define CP_UTF7                   65000       // UTF-7 translation
#define CP_UTF8                   65001       // UTF-8 translation

#define MB_PRECOMPOSED            0x00000001  // use precomposed chars
#define MB_COMPOSITE              0x00000002  // use composite chars
#define MB_USEGLYPHCHARS          0x00000004  // use glyph chars, not ctrl chars
#define MB_ERR_INVALID_CHARS      0x00000008  // error for invalid chars


#define STYPE_DISKTREE          0
#define STYPE_PRINTQ            1
#define STYPE_DEVICE            2
#define STYPE_IPC               3

#define SHARE_NETNAME_PARMNUM         1
#define SHARE_TYPE_PARMNUM            3
#define SHARE_REMARK_PARMNUM          4
#define SHARE_PERMISSIONS_PARMNUM     5
#define SHARE_MAX_USES_PARMNUM        6
#define SHARE_CURRENT_USES_PARMNUM    7
#define SHARE_PATH_PARMNUM            8
#define SHARE_PASSWD_PARMNUM          9
#define SHARE_FILE_SD_PARMNUM       501

#define STYPE_SPECIAL                   2147483648


#define ACCESS_READ         0x01
#define ACCESS_WRITE        2
#define ACCESS_CREATE       4
#define ACCESS_EXEC         8
#define ACCESS_DELETE       16
#define ACCESS_ATRIB        32
#define ACCESS_PERM         64

#define ACCESS_GROUP        32768

#define ACCESS_ALL          (ACCESS_READ+ACCESS_WRITE+ACCESS_CREATE+ACCESS_EXEC+ACCESS_DELETE+ACCESS_ATRIB+ACCESS_PERM)

#define SHI_USES_UNLIMITED      -1

//
// Flags values for the 501, 1005, and 1007 infolevels
//
#define SHI1005_FLAGS_DFS       1    // Share is in the DFS
#define SHI1005_FLAGS_DFS_ROOT  2    // Share is root of DFS

#define COW_PERMACHINE          4    // Share data is per-machine data
#define COW_PERUSER             8    // Share data is per-user data

#define CSC_CACHEABLE           16    // Client can cache files for off-line access
#define CSC_NOFLOWOPS           32    // Client need not flow operations to the server
#define CSC_AUTO_INWARD         64    // Auto inward propagation (server->client) w/o UI
#define CSC_AUTO_OUTWARD        128    // Auto outward propagation(client->server) w/o UI

prototype LONG netapi32.NetShareAdd( POINTER, LONG, POINTER, POINTER );
prototype LONG netapi32.NetShareDel( POINTER, POINTER, LONG ); 
prototype LONG kernel32.MultiByteToWideChar(LONG,LONG,POINTER,LONG,POINTER,LONG);

typedef _SHARE_INFO_2
begin
    LPSTR        shi2_netname;
    NUMBER       shi2_type;
    LPSTR        shi2_remark;
    NUMBER       shi2_permissions;
    NUMBER       shi2_max_uses;
    NUMBER       shi2_current_uses;
    LPSTR        shi2_path;
    LPSTR        shi2_passwd;
end;

typedef _WIDESTRING
begin
    STRING          Wide[400];
end;

prototype _NetShareAdd(STRING,STRING,STRING);
prototype _NetShareDel(STRING);


#endif

